<?php

namespace MiamiOH\RestngVisa\Services;

use Exception;
use function strtotime;

class Visa extends \MiamiOH\RESTng\Service {

    private $dbDataSourceName = 'MUWS_GEN_PROD';
    private $dbh;

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    public function setBannerUtil($bannerUtil)
    {
        /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    public function getVisas()
    {

        $request = $this->getRequest();
        $response = $this->getResponse();

        $idType = $request->getResourceParamKey('muid');

        $id = $request->getResourceParam('muid');
        $options = $request->getOptions();

        try{
            $person = $this->bannerUtil->getId($idType, $id);
            $pidm = $person->getPidm();
        }catch(\Exception $e){
             $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
             return $response;
        }

        $payload = $this->getVisaInformation($pidm, $options['return']);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
  }

    private function getVisaInformation($pidm, $selectedOption)
    {

        $statement = "select
                          gorvisa_pidm,
                          gorvisa_vtyp_code,
                          stvvtyp_desc,
                          gorvisa_visa_number,
                          gorvisa_natn_code_issue,
                          stvnatn_nation,
                          gorvisa_visa_start_date,
                          gorvisa_visa_expire_date
                        from gorvisa 
                        inner join stvvtyp on gorvisa_vtyp_code = stvvtyp_code
                        left join stvnatn on gorvisa_natn_code_issue = stvnatn_code
                        where gorvisa_pidm = ?";


        $payload = $this->dbh->queryall_array($statement, $pidm);
        $currentDate = date("d-M-y");

        $formattedRecords = [];


        foreach ($payload as $record){
           if($selectedOption == 'active'){
               if((strtotime($record['gorvisa_visa_expire_date']) > strtotime($currentDate))
                   && (strtotime($currentDate) > strtotime($record['gorvisa_visa_start_date']))){
                   $formattedRecords[] =  $this->formatRecord($record);
               }
           }else if($selectedOption == 'expired'){
               if(strtotime($record['gorvisa_visa_expire_date']) < strtotime($currentDate)){
                   $formattedRecords[] =  $this->formatRecord($record);
               }
           }else if($selectedOption == 'future'){
               if(strtotime($record['gorvisa_visa_start_date']) > strtotime($currentDate)){
                   $formattedRecords[] =  $this->formatRecord($record);
               }
           }else{
               $formattedRecords[] =  $this->formatRecord($record);
           }
        }

        return $formattedRecords;
    }

    private function formatRecord($record)
    {
        $formattedRecord['pidm'] = $record['gorvisa_pidm'];
        $formattedRecord['visaTypeCode'] = $record['gorvisa_vtyp_code'];
        $formattedRecord['visaType'] = $record['stvvtyp_desc'];
        $formattedRecord['visaNumber'] = $record['gorvisa_visa_number'];
        $formattedRecord['issuingNationCode'] = $record['gorvisa_natn_code_issue'];
        $formattedRecord['issuingNation'] = $record['stvnatn_nation'];
        $formattedRecord['visaStartDate'] = $record['gorvisa_visa_start_date'];
        $formattedRecord['visaExpireDate'] = $record['gorvisa_visa_expire_date'];

        return $formattedRecord;
    }
}
