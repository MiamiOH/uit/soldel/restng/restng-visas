<?php

namespace MiamiOH\RestngVisa\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class VisaResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition([
            'name' => 'person.visa.model',
            'type' => 'object',
            'properties' => [
                'visaTypeCode' => [
                    'type' => 'string'
                ],
                'visaType' => [
                    'type' => 'string'
                ],
                'visaNumber' => [
                    'type' => 'integer'
                ],
                'issuingNationCode' => [
                    'type' => 'string'
                ],
                'issuingNation' => [
                    'type' => 'string'
                ],
                'visaStartDate' => [
                    'type' => 'string'
                ],
                'visaExpireDate' => [
                    'type' => 'string'
                ],
            ]
        ]);

        $this->addDefinition(array(
            'name' => 'person.visa.collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/person.visa.model'
            ]
        ));

    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => 'Visa',
            'class' => 'MiamiOH\RestngVisa\Services\Visa',
            'description' => 'Information about visas',
            'set' => [
                'database' => ['type' => 'service', 'name' => 'APIDatabaseFactory'],
                'bannerUtil' => ['type' => 'service', 'name' => 'MU\BannerUtil'],
            ]
        ]);

    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'name' => 'person.visa',
            'tags' => ['Person'],
            'description' => 'Get visas for a person.',
            'pattern' => '/person/visa/v1/:muid',
            'service' => 'Visa',
            'method' => 'getVisas',
            'params' => [
                'muid' => [
                    'description' => "A Person's uniqueID or pidm",
                    'alternateKeys' => ['uniqueId', 'pidm']
                ]
            ],
            'options' => [
                'return' => [
                    'enum' => ['all', 'active', 'expired', 'future'],
                    'default' => 'active',
                    'description' => 'Filter visas by expiration',
                ]
            ],
            'middleware' => [
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => 'Person-Visas',
                    'key' => 'view'
                ],
                'authenticate' => ['type' => 'token']
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Visa for a person',
                    'returns' => [
                        'type' => 'collection',
                        '$ref' => '#/definitions/person.visa.collection',
                    ]
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'The user could not be authenticated or is not authorized to access the resource.'
                ],
                App::API_BADREQUEST => [
                    'description' => 'The request was incorrectly formed.'
                ],
                App::API_FAILED => [
                    'description' => 'Internal server error',
                ]
            ]

        ]);

    }

    public function registerOrmConnections(): void
    {

    }
}