## README.md
A README.md file has been added to the root of your project (you are reading it right now).  If this causes problems for the deployment of your project, please consider re-organizing your project so that it can remain.  For instance, if this is a web development project and the root of this project gets served via the webserver, consider re-organizing this project to have a www/ directory in the root and place your web files there.  This makes it so that the root of your project can contain directories like sql/, docs/, and tests/ that don't get deployed to the webserver. You will need to adjust your deployment instructions as appropriate.

This README.md file is also displayed in GitLab when browsing projects.  Consider replacing the content of this document with something appropriate to your project, like a summary, simple documentation, links to more complex documentation, deployment instructions, etc.

## Git Resources
* [Main Git/GitLab @ Miami Page](https://wiki.miamioh.edu/confluence/display/Doc/Git+and+GitLab)
* [Mastering Git Video Training](https://wiki.miamioh.edu/confluence/display/Doc/Mastering+Git+Video+Collection)
* [Git Workbook](https://wiki.miamioh.edu/confluence/display/Doc/Git+Workbook)

