<?php
/**
 * Created by PhpStorm.
 * User: vallamsd
 * Date: 5/14/18
 * Time: 1:26 PM
 */

namespace MiamiOH\RestngVisa\Tests\Unit;

use MiamiOH\RESTng\App;

class VisasTest extends  \MiamiOH\RESTng\Testing\TestCase{
    private $mockApp;
    private $dbh;
    private $request;
    private $visa;
    private $bannerUtil;

    protected function setUp()
    {

        $this->mockApp = $this->createMock(App::class);

        $this->mockApp->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions','getResourceParamKey'))
            ->getMock();

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();


        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(['getHandle'])
            ->getMock();

        $this->bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(['getId', 'getPidm', 'getUniqueId'])
            ->getMock();

        $db->method('getHandle')
            ->willReturn($this->dbh);

        $this->visa = new \MiamiOH\RestngVisa\Services\Visa();
        $this->visa->setApp($this->mockApp);
        $this->visa->setDatabase($db);
        $this->visa->setBannerUtil($this->bannerUtil);
        $this->visa->setRequest($this->request);
    }

    public function testCanGetAllVisaInformationForAGivenPerson()
    {
        $this->request->method('getResourceParam')
             ->with('muid')
             ->willReturn('rtr3450');

        $this->request->method('getResourceParamKey')
             ->with('muid')
             ->willReturn('uniqueId');

        $this->request->method('getOptions')
             ->willReturn([
                 'return' => 'all'
             ]);

        $this->bannerUtil->method('getId')
            ->willReturn($this->bannerUtil);

        $this->bannerUtil->method('getPidm')
             ->willReturn('1234567');

        $this->dbh->method('queryall_array')
            ->willReturn(
               $this->mockQueryVisa()
            );

        $this->response = $this->visa->getVisas();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($this->mockQueryVisaPayload(), $this->response->getPayload());
    }


    public function testCanGetExpiredVisaInformationForAGivenPerson()
    {
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('rtr3450');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('uniqueId');

        $this->request->method('getOptions')
            ->willReturn([
                'return' => 'expired'
            ]);

        $this->bannerUtil->method('getId')
            ->willReturn($this->bannerUtil);

        $this->bannerUtil->method('getPidm')
            ->willReturn('1234567');

        $this->dbh->method('queryall_array')
            ->willReturn(
                $this->mockQueryForExpiredVisa()
            );

        $this->response = $this->visa->getVisas();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($this->mockQueryExpiredVisaPayload(), $this->response->getPayload());
    }

    public function testWillGetNoVisaInformationForAGivenPersonWhenTheyHaveExpiredRecordAndActiveOptionIsSelected()
    {
        $this->request->method('getResourceParam')
            ->with('muid')
            ->willReturn('rtr3450');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            ->willReturn('uniqueId');

        $this->request->method('getOptions')
            ->willReturn([
                'return' => 'active'
            ]);

        $this->bannerUtil->method('getId')
            ->willReturn($this->bannerUtil);

        $this->bannerUtil->method('getPidm')
            ->willReturn('1234567');

        $this->dbh->method('queryall_array')
            ->willReturn(
                $this->mockQueryForExpiredVisa()
            );

        $this->response = $this->visa->getVisas();
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $this->response->getStatus());
        $this->assertEquals($this->mockQueryExpiredVisa(), $this->response->getPayload());
    }

    private function mockQueryVisa()
    {
        return [
            [
                'gorvisa_pidm'             => '1234567',
                'gorvisa_vtyp_code'        => 'F1',
                'stvvtyp_desc'             => 'Student',
                'gorvisa_visa_number'      => '123456F1',
                'gorvisa_natn_code_issue'  => 'IND',
                'stvnatn_nation'           => 'INDIA',
                'gorvisa_visa_start_date'  => '15-APR-16',
                'gorvisa_visa_expire_date' => '15-APR-21',
            ]
        ];
    }


    private function mockQueryForExpiredVisa()
    {
        return [
            [
                'gorvisa_pidm'             => '1234567',
                'gorvisa_vtyp_code'        => 'F1',
                'stvvtyp_desc'             => 'Student',
                'gorvisa_visa_number'      => '123456F1',
                'gorvisa_natn_code_issue'  => 'IND',
                'stvnatn_nation'           => 'INDIA',
                'gorvisa_visa_start_date'  => '15-APR-16',
                'gorvisa_visa_expire_date' => '15-APR-18',
            ]
        ];
    }

    private function mockQueryExpiredVisaPayload()
    {
        return [
             [
                'pidm'              => '1234567',
                'visaTypeCode'      => 'F1',
                'visaType'          => 'Student',
                'visaNumber'        => '123456F1',
                'issuingNationCode' => 'IND',
                'issuingNation'     => 'INDIA',
                'visaStartDate'     => '15-APR-16',
                'visaExpireDate'    => '15-APR-18'
            ]
        ];
    }
    private function mockQueryVisaPayload()
    {
        return [
             [
                'pidm'              => '1234567',
                'visaTypeCode'      => 'F1',
                'visaType'          => 'Student',
                'visaNumber'        => '123456F1',
                'issuingNationCode' => 'IND',
                'issuingNation'     => 'INDIA',
                'visaStartDate'     => '15-APR-16',
                'visaExpireDate'    => '15-APR-21'
            ]
        ];
    }

    private function mockQueryExpiredVisa()
    {
        return [];
    }
}